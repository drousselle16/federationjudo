package web.controleurs;

import donnees.Dao;
import entites.Club;
import entites.Judoka;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import utilitaires.UtilDojo;
import utilitaires.UtilTriListe;

@ManagedBean
@ViewScoped
public class Controleur01 implements Serializable {
     
    @Inject Dao dao;
    
    private String       codeclub;
    
    private Club         club;
  
    public void rechercheClub(){
            
        if (codeclub != null) {
            club= dao.getClubDeCode(codeclub);
            UtilTriListe.trierLesJudokasParNomPrenom(club.getLesJudokas());
        }
    }
    
    public String codeCouleurCeinture(Judoka j){
    
         return UtilDojo.couleurHexaCeinture(j.getCeinture());      
    }
       
    //<editor-fold defaultstate="collapsed" desc="gets sets">
    
    public String getCodeclub() {
        return codeclub;
    }
    
    public void setCodeclub(String codeclub) {
        this.codeclub = codeclub;
    }
    
    public Club getClub() {
        return club;
    }
    
    public void setClub(Club club) {
        this.club = club;
    }
    
    //</editor-fold> 
}