package web.controleurs;

import donnees.Dao;
import entites.Judoka;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import utilitaires.UtilDojo;

@ManagedBean
@ViewScoped
public class Controleur02 implements Serializable {
     
      @Inject Dao dao;
    
    private Long         idJudoka; 
    private Judoka       judoka;
  
    public void rechercheJudoka(){
            
        if (idJudoka != null) judoka= dao.getJudokaNumero(idJudoka);
    }
    
//    public String codeCouleurCeinture(){
//    
//         return judoka!=null?UtilDojo.couleurHexaCeinture(judoka.getCeinture()):"";      
//    }
    
    //<editor-fold defaultstate="collapsed" desc="gets sets">
    
     public Long getIdJudoka() {
        return idJudoka;
    }

    public void setIdJudoka(Long idJudoka) {
        this.idJudoka = idJudoka;
    }

    public Judoka getJudoka() {
        return judoka;
    }

    public void setJudoka(Judoka judoka) {
        this.judoka = judoka;
    }
    
    //</editor-fold> 
 
}


