package donnees;

import entites.Club;
import entites.Judoka;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class Dao {
      
  @PersistenceContext  
  private EntityManager  em;  
 
 public List<Judoka>    getTousLesJudokas() {
     
     return em.createQuery("Select j from Judoka j order by j.nom , j.prenom").getResultList();
 }
 
 public  List<Club>      getTousLesClubs() {
     
      return em.createQuery("Select c from Club c order by c.nomClub").getResultList();
 }
 
 public  Judoka          getJudokaNumero(Long pId){
     
     return em.find(Judoka.class, pId);
 }
 
 public  Club            getClubDeCode (String pCode){
     
     return em.find(Club.class,pCode);
 }
 
 public  void            enregister(Object entite){
 

     em.persist(entite);
   
 }
 public  void            modifier(Object entite){
 
    
     entite=em.merge(entite);
   
 }
 public  void            supprimer(Object entite) {
      
     
     em.remove(entite);
     
        
    }
}

